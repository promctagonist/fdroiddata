Categories:System
License:GPLv3
Web Site:https://sites.google.com/site/ghostcommander1
Source Code:http://sourceforge.net/p/ghostcommander/code
Issue Tracker:http://sourceforge.net/projects/ghostcommander/support
Donate:http://sourceforge.net/p/ghostcommander/donate

Auto Name:Ghost Commander
Summary:Dual-panel file manager
Description:
Dual panel file manager, like Norton Commander, Midnight Commander or Total
Commander.

Notable features:

* Ability to copy and move files between its two panels
* Create or extract (unzip) ZIP archives
* Transfer files via SFTP - plugin required: [[com.ghostsq.commander.sftp]]
* Transfer files via SMB - plugin required: [[com.ghostsq.commander.samba]]
* Full-featured app manager able to access extra info and share apks

Requires root: No, but if provided you can remount filesystems and mess around
with system files.
.

Repo Type:srclib
Repo:GhostCommander

# All commit messages are empty, so I'm just guessing the revision.
Build:1.35,94
    commit=131

Build:1.35.1b5,97
    commit=141

Build:1.36.4,110
    commit=161

Build:1.40,160
    commit=302

Build:1.40.1,163
    commit=307

Build:1.40.2,164
    commit=314

# DexClassLoader seems to be used for the samba/sftp plugins
Build:1.43.3b1,210
    commit=407
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.50.1,214
    commit=387
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51b2,219
    commit=390
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51b6,223
    commit=398
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51b7,224
    commit=402
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51.4,233
    disable=Build fails - missing R.drawable.plugins
    commit=412
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51.5b3,236
    disable=skip beta

Build:1.52.1,246
    commit=427
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.52.2,250
    commit=445
    target=android-19
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.52.5b3,270
    commit=500
    target=android-19
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.52.6b1,276
    commit=507
    target=android-19
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.53b1,279
    commit=509
    srclibs=TouchImageView@6dbeac4f11936185ba374c73144ac431c23c9aab
    extlibs=android/android-support-v4.jar
    prebuild=cp $$TouchImageView$$/src/com/ortiz/touch/TouchImageView.java src/
    target=android-21
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.53b2,280
    commit=511
    extlibs=android/android-support-v4.jar
    target=android-21
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.53b3,281
    commit=514
    extlibs=android/android-support-v4.jar
    target=android-21
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.53b4,282
    commit=516
    extlibs=android/android-support-v4.jar
    target=android-21
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.53b5,283
    commit=517
    extlibs=android/android-support-v4.jar
    target=android-21
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.53b6,284
    commit=518
    extlibs=android/android-support-v4.jar
    target=android-21
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.53,285
    commit=519
    extlibs=android/android-support-v4.jar
    target=android-21
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.53.1b1,286
    commit=522
    extlibs=android/android-support-v4.jar
    target=android-21
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.53.1,287
    commit=523
    extlibs=android/android-support-v4.jar
    target=android-21
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Maintainer Notes:
No commit messages in source repo! No tags. Apks on website can be used to
confirm a release is real. Commit where AndroidManifest is changed usually
corresponds to version.

The scanignore is for the dex class loading which is used for loading plugins.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
# Update Check Ignore:b
Current Version:1.53.1
Current Version Code:287
