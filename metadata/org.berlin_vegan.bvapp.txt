Categories:Navigation
License:GPLv2
Web Site:http://www.berlin-vegan.org/app/index_en.html
Source Code:https://github.com/smeir/Berlin-Vegan-Guide
Issue Tracker:https://github.com/smeir/Berlin-Vegan-Guide/issues

Auto Name:Berlin Vegan
Summary:Guide for vegan dining and shopping in Berlin
Description:
Quick access to an overview of the vegan food and shopping possibilities in
Berlin, Germany -- even while you are out and about. We have arranged all the
information and divided it up into frequently used categories. After you have
selected a category (e.g. 'restaurants'), the results will be automatically
sorted and shown in order of distance from your current location. With the help
of the number of stars, you can also see how vegan- friendly a place is
considered to be.

What is more, the guide takes opening hours into consideration (very practical
when you need fresh bread rolls on a Sunday morning!) and also allows you to
activate your own filters, such as "wheelchair-friendly", "dogs allowed" or
"organic".

The guide is not just suitable for vegetarians and vegans, but also for all
people who are trying to live their life in a greener and more animal friendly
way.

The app contains the following information:

* Over 200 restaurants, snack bars and takeaways with detailed descriptions
* 230 bakeries with a complete description of what is on offer
* A further 200 shopping possibilities in the area of cosmetics, groceries and organic produce
* More than 100 cafes with soy milk available
* A "vegan passport" with a detailed description for the staff at restaurants and hotels, the text for which has been kindly put together for us by the Vegan Society
.

Repo Type:git
Repo:https://github.com/smeir/Berlin-Vegan-Guide

Build:1.6,18
    commit=116c661d029f7f23cb2be4ad168dc8554dd16794
    subdir=platforms/android
    init=echo "# Just create the file" > project.properties
    srclibs=1:Cordova@3.2.0,YUICompressor@v2.4.8
    rm=lib/tools/ycompressor.jar,platforms/android/libs/cordova*jar,platforms/android/libs/phonegap*jar,platforms/android/local.properties,platforms/android/build.properties,platforms/android/default.properties,platforms/android/build.xml
    target=android-19
    build=pushd $$YUICompressor$$ && \
        ant && \
        popd && \
        cp $$YUICompressor$$/build/yuicompressor-2.4.8.jar ../../lib/tools/ycompressor.jar && \
        cd ../../ && \
        ant prod.android

Build:1.9,21
    commit=47f0850acf58e80e7f0d8ec75251408d6bcefddb
    subdir=platforms/android
    init=echo "# Just create the file" > project.properties
    srclibs=1:Cordova@3.2.0,YUICompressor@v2.4.8
    rm=lib/tools/ycompressor.jar,platforms/android/libs/cordova*jar,platforms/android/libs/phonegap*jar,platforms/android/local.properties,platforms/android/build.properties,platforms/android/default.properties,platforms/android/build.xml
    target=android-19
    build=pushd $$YUICompressor$$ && \
        ant && \
        popd && \
        cp $$YUICompressor$$/build/yuicompressor-2.4.8.jar ../../lib/tools/ycompressor.jar && \
        cd ../../ && \
        ant prod.android

Maintainer Notes:
* YUICompressor has jars.
* Remove "init" on following versions when we fixed regression in fdroidserver.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.9
Current Version Code:21
