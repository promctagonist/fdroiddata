Categories:Time
License:Apache2
Web Site:https://github.com/jonasbleyl/Recurrence/blob/HEAD/README.md
Source Code:https://github.com/jonasbleyl/Recurrence
Issue Tracker:https://github.com/jonasbleyl/Recurrence/issues

Auto Name:Recurrence
Summary:Get reminded about notifications
Description:
Get reminded about notifications.

Features:

* Repeat reminders daily, weekly, monthly or even yearly.
* Decide exactly how many times you want a notification to appear.
* Snooze notifications to have them remind you again at a later time.
* View all of your upcoming and previous notification reminders.
* Add icons and colors to your notifications.
.

Repo Type:git
Repo:https://github.com/jonasbleyl/Recurrence

Build:1.1,10
    commit=877501c74635ff1d82f0f54f0dd0338e28d6a1a4
    subdir=app
    gradle=yes

Build:1.1.1,11
    commit=c379e7254f62cf81c3b56f4642f33a14b37f549e
    subdir=app
    gradle=yes

Build:1.2,12
    commit=40c35d63fa970fd8f79ee457952d79418cf69e1c
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2.2
Current Version Code:14
