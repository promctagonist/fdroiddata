Categories:Security
License:GPLv3+
Web Site:https://secupwn.github.io/Android-IMSI-Catcher-Detector
Source Code:https://github.com/SecUpwN/Android-IMSI-Catcher-Detector
Issue Tracker:https://github.com/SecUpwN/Android-IMSI-Catcher-Detector/issues
Changelog:https://github.com/SecUpwN/Android-IMSI-Catcher-Detector/blob/HEAD/CHANGELOG.md
Donate:https://www.bountysource.com/teams/android-imsi-catcher-detector/issues

Auto Name:Android IMSI-Catcher Detector
Summary:Detect and avoid fake base stations
Description:
Detect fake base stations (IMSI-Catchers) in GSM/UMTS Networks. Please visit the
website for detailed information.
.

Repo Type:git
Repo:https://github.com/SecUpwN/Android-IMSI-Catcher-Detector.git

Build:0.1.24-alpha,24
    commit=eddf37469e48acf194c852d2da81875c650ae052
    subdir=app
    gradle=yes

Build:0.1.25-alpha-b6,25
    commit=v0.1.25-alpha-b6
    subdir=app
    gradle=yes

Build:0.1.26-alpha-b00,26
    commit=v0.1.26-alpha-b00
    subdir=app
    gradle=yes
    srclibs=OSMBonusPack@v5.1
    rm=app/libs/*.jar
    prebuild=sed -i -e '/osmbonuspack/d' build.gradle && \
        cp -fR $$OSMBonusPack$$/src/org src/main/java/

Build:0.1.27-alpha-b00,27
    commit=v0.1.27-alpha-b00
    subdir=app
    gradle=yes
    srclibs=OSMBonusPack@v5.1
    rm=app/libs/*.jar
    prebuild=sed -i -e '/osmbonuspack/d' build.gradle && \
        cp -fR $$OSMBonusPack$$/src/org src/main/java/

Build:0.1.28-alpha-b00,28
    commit=v0.1.28-alpha-b00
    subdir=app
    gradle=yes
    srclibs=OSMBonusPack@v5.1
    rm=app/libs/*.jar
    prebuild=sed -i -e '/osmbonuspack/d' build.gradle && \
        cp -fR $$OSMBonusPack$$/src/org src/main/java/

Build:0.1.30-alpha-b00,30
    commit=v0.1.30-alpha-b00
    subdir=app
    gradle=yes
    srclibs=OSMBonusPack@v5.1
    rm=app/libs/*.jar
    prebuild=sed -i -e '/osmbonuspack/d' build.gradle && \
        cp -fR $$OSMBonusPack$$/src/org src/main/java/

Build:0.1.31-alpha-b00,31
    commit=v0.1.31-alpha-b00
    subdir=app
    gradle=yes
    srclibs=OSMBonusPack@v5.1
    rm=app/libs/*.jar
    prebuild=sed -i -e '/osmbonuspack/d' build.gradle && \
        cp -fR $$OSMBonusPack$$/src/org src/main/java/

Build:0.1.32-alpha-b00,32
    commit=v0.1.32-alpha-b00
    subdir=app
    gradle=yes
    srclibs=OSMBonusPack@v5.1
    rm=app/libs/*.jar
    prebuild=sed -i -e '/osmbonuspack/d' build.gradle && \
        cp -fR $$OSMBonusPack$$/src/org src/main/java/

Build:0.1.33-alpha-b00,33
    commit=v0.1.33-alpha-b00
    subdir=app
    gradle=yes
    srclibs=OSMBonusPack@v5.1
    rm=app/libs/*.jar
    prebuild=sed -i -e '/osmbonuspack/d' build.gradle && \
        cp -fR $$OSMBonusPack$$/src/org src/main/java/

Build:0.1.34-alpha-b00,34
    commit=v0.1.34-alpha-b00
    subdir=app
    gradle=yes
    srclibs=OSMBonusPack@v5.1
    rm=app/libs/*.jar
    prebuild=sed -i -e '/osmbonuspack/d' build.gradle && \
        cp -fR $$OSMBonusPack$$/src/org src/main/java/

Build:0.1.35-alpha-b00,35
    disable=encoding error
    commit=v0.1.35-alpha-b00
    subdir=app
    gradle=yes
    srclibs=OSMBonusPack@v5.1
    rm=app/libs/*.jar
    prebuild=sed -i -e '/osmbonuspack/d' build.gradle && \
        cp -fR $$OSMBonusPack$$/src/org src/main/java/

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.1.35-alpha-b00
Current Version Code:35
