Categories:Multimedia
License:GPLv3
Web Site:https://olejon.github.io/spotcommander
Source Code:https://github.com/olejon/spotcommander
Issue Tracker:https://github.com/olejon/spotcommander/issues
Donate:http://www.olejon.net/code/spotcommander/?donate

Auto Name:SpotCommander
Summary:Remote control for Spotify for Linux
Description:
Remote control Spotify for Linux. For this to work, you have to install
[http://www.olejon.net/code/spotcommander/?install SpotCommander] on your
regular PC first.
.

Repo Type:git
Repo:https://github.com/olejon/spotcommander

Build:4.7,47
    commit=c468892825cc300889fcb105223e10f271745e98
    subdir=android/app
    gradle=yes

Build:4.8,48
    commit=df0858c0c35c753d5fa338425e477174fa2271cd
    subdir=android/app
    gradle=yes

Build:4.9,49
    commit=6bb20e9cf0a73665b9d2583eb3267b34d0ed3c16
    subdir=android/app
    gradle=yes

Build:5.0,50
    commit=cc2a256055a7b5a0ec389ef16eb49d98815ca772
    subdir=android/app
    gradle=yes

Build:5.1,51
    commit=4353eca0a7ab2b82253ce403dad9bf1b1980ad47
    subdir=android/app
    gradle=yes

Build:5.2,52
    commit=ac233fee01fc1b347a37ccb10828c22bb1a34839
    subdir=android/app
    gradle=yes

Build:5.4,54
    commit=ab43d9685c70f51ab0e285c80eec8d087230d983
    subdir=android/app
    gradle=yes

Build:5.5,55
    commit=3a28b8d585cf6879f7a1989c6ce8b5d6b1cdb640
    subdir=android/app
    gradle=yes

Build:5.6,56
    commit=f81bacce7415bd829dcc7daef3f440534fd5851a
    subdir=android/app
    gradle=yes

Build:5.8,58
    commit=66fc751fd8f694a681f54fcd2d0833da27d31a35
    subdir=android/app
    gradle=yes

Build:5.9,59
    commit=2bf8a087bc13d2cc57bd985b947d823ae941844a
    subdir=android/app
    gradle=yes

Build:6.0,60
    commit=9a8a3ad703eebab9003a3b721df1f74d30df02ae
    subdir=android/app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:6.1
Current Version Code:61
