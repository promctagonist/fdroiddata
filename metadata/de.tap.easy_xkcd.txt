Categories:Reading
License:Apache2
Web Site:
Source Code:https://github.com/T-Rex96/Easy_xkcd
Issue Tracker:https://github.com/T-Rex96/Easy_xkcd/issues

Auto Name:Easy xkcd
Summary:View xkcd comics
Description:
A fast and beautiful way to view your favorite [https://xkcd.com/ xkcd] comics.

Comic Browser:

* Offline Mode
* Notifications
* Long press to view alt text
* Search for title, transcript or number
* Share comic url or image
* Add comic to favorites
* Favorites are saved for offline use
* Open links from xkcd.com and m.xkcd.com
* Support for large images (e.g comic 657)
* Explain xkcd integration
* Option to display alt text by default

What If?:

* Full support for footnotes, formulas and alt text
* Offline mode and notifications
* Mark articles as read and hide them
* Swipe between articles (disabled by default)
* Night mode
* Quick search for article titles
* Random articles

General:

* Themes (blue, green, black...)
* Material design elements like Snackbars, Floating Action Button, tinted Status Bar, animations...
* Lock orientation
.

Repo Type:git
Repo:https://github.com/T-Rex96/Easy_xkcd

Build:1.2.3,10
    commit=581f45e473a1993500887d0aeaceb6a559be8d88
    subdir=app
    gradle=yes

Build:1.2.5,12
    commit=78ff7e8f281193198b0178660108c97703e98147
    subdir=app
    gradle=yes

Build:1.3,13
    commit=695d48c128450b6811ca742b667d74a9d370f9b7
    subdir=app
    gradle=yes

Build:1.4.1,15
    commit=28bcd0f3f437779816f45aa85389afc28f45eb39
    subdir=app
    gradle=yes

Build:1.4.2,16
    commit=f6c5838407e7964aa9832e291be7935297d3e00e
    subdir=app
    gradle=yes

Build:1.4.3,17
    commit=8f1afd0fe4c90ae818f866b025bfb8d909739c51
    subdir=app
    gradle=yes

Build:1.4.4,18
    commit=9fb625c4845f11289f07c90e0a9de7f3e63b2775
    subdir=app
    gradle=yes

Build:1.5.1,19
    commit=6c423f9e7a20dfaee477a1613e01d5c702669434
    subdir=app
    gradle=yes

Build:1.5.2,20
    commit=8ce6196b7bbc1065330fddbe6972ec7dfd0428fc
    subdir=app
    gradle=yes

Build:1.6,21
    commit=ba9fd6597c42de2d747f2b77722ea1828daf3061
    subdir=app
    gradle=yes

Build:1.6.2,23
    commit=14057157b3805741d1d464f5da2622b8fb22f28c
    subdir=app
    gradle=yes

Build:1.7.3,27
    commit=f7363b71d750a346b4ccc3e553a7ffdc857cea18
    subdir=app
    gradle=yes

Build:2.0,31
    commit=8fcaa09c98b6f88be1bcdc1fcdc9e668d4fa22a1
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.1,36
    commit=1719abf5d1a089728e502a03da6cc5a0f74b115d
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.2,38
    commit=5851fea54b00c1b735a281dded647585c6ef0634
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.3.1,39
    commit=c843639cd3b562ae3615fc742a32272f842c526d
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.3.5,44
    commit=dd9fd625affa13a62f4555054c3f27986439c864
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.3.6,45
    commit=e5eb41762a3c3b3b0d7a18e404379301de486e39
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.4.2,51
    commit=2a6b4d77dd1740b69c4eb99a5303d637d886c550
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.5,53
    commit=427dc6d23af52b4ea64c2d93e7021cebac0e2d4f
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.5.3,56
    commit=3fe26abf1cb3c080ff9e827991aa936d639a82bc
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.6,57
    commit=b68cd19f6dd2930cfff39dc8371da3019f02ec86
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.7.1,59
    commit=release_2.7.1
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.7.3,61
    commit=release_2.7.3
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.8.1,63
    commit=release_2.8.1
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.8.2,64
    commit=release_2.8.2
    subdir=app
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -fR $$CWACWakeful$$/wakeful ../ && \
        sed -i -e '/maven {/,+2d' -e '/jcenter/amavenLocal()' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/acompile project(":wakeful")' build.gradle && \
        echo 'include ":wakeful"' >> ../settings.gradle && \
        sed -i -e '/android.libraryVariants.all/,$d' ../wakeful/build.gradle

Build:2.9,65
    commit=release_2.9
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.0,66
    commit=release_3.0
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.1,67
    commit=release_3.1
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.2.1,69
    commit=release_3.2.1
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.3,70
    commit=release_3.3
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.3.2,72
    commit=release_3.3.2
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.3.3,73
    commit=release_3.3.3
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.3.4,74
    commit=release_3.3.4
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.3.7,77
    commit=release_3.3.7
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.3.8,78
    commit=release_3.3.8
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Build:3.3.9,79
    commit=release_3.3.9
    subdir=app
    init=sed -i -e '/maven {/,+2d' -e '/com.commonsware.cwac:wakeful/d' -e '/fileTree/d' build.gradle
    gradle=yes
    srclibs=CWACWakeful@v1.0.5
    prebuild=cp -r $$CWACWakeful$$/wakeful/src/com src/main/java/

Auto Update Mode:Version release_%v
Update Check Mode:Tags ^release
Current Version:3.3.9
Current Version Code:79
