Categories:System
License:GPLv3+
Web Site:https://f-droid.org
Source Code:https://gitlab.com/fdroid/fdroidclient
Issue Tracker:https://gitlab.com/fdroid/fdroidclient/issues
Donate:https://f-droid.org/about
FlattrID:343053
Bitcoin:15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18

Auto Name:F-Droid Privileged Extension
Summary:Help F-Droid acquire system privileges
Description:
'''Note:''' This is still in a testing phase. Current F-Droid versions do not
yet interact with this extension.

[[org.fdroid.fdroid]] can make use of system privileges or permissions to
install, update and remove applications on its own. The only way to obtain those
privileges is to become a system app.

This is where the Privileged Extension comes in - being a separate app and much
smaller, it can be installed as a system app and communicate with the main app
via AIDL IPC.

This has several advantages:

* Reduced disk usage in the system partition
* System updates don't remove F-Droid
* The process of installing into system via root is safer
.

Repo Type:srclib
Repo:fdroidclient

Build:0.1-alpha0,1000
    commit=68899ec476a20de87389e2ac1b38aa5463c4c35e
    subdir=F-Droid-Privileged
    gradle=yes

Auto Update Mode:None
Update Check Mode:Static
Current Version:0.1-alpha0
Current Version Code:1000
