Categories:Internet
License:GPLv2+
Web Site:https://github.com/Nyubis/Pomfshare/blob/HEAD/README.md
Source Code:https://github.com/Nyubis/Pomfshare
Issue Tracker:https://github.com/Nyubis/Pomfshare/issues

Auto Name:Pomfshare
Summary:Client for pomf.se
Description:
Pomshare uses Android's built-in sharing menu to easily upload files to
[http://pomf.se pomf.se], a free quality filehost. No timeouts, no logins. 50
MiB maximum.
.

Repo Type:git
Repo:https://github.com/Nyubis/Pomfshare

Build:1.0,1
    commit=ae6a909ebc10a4726dd4fee833b6eb680dfb928c

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1
