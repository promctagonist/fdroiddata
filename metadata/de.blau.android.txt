Categories:Navigation
License:Apache2
Web Site:https://marcuswolschon.github.io/osmeditor4android
Source Code:https://github.com/MarcusWolschon/osmeditor4android
Issue Tracker:https://github.com/MarcusWolschon/osmeditor4android/issues

Auto Name:Vespucci
Summary:OpenStreetMap editor
Description:
* Create and edit new Nodes and Ways
* Append Nodes to existing Ways
* Delete Nodes
* Create, edit and delete Tags
* Download and Upload to OSM-Server
* Highlight unnamed highways, and ways/nodes with TODOs or FIXMEs
* Add, comment and close OpenStreetBugs
* Use a variety of background tile layers as reference
* Show the users GPS-Track with accuracy
* Display the raw data

What is Vespucci NOT?

* a map-view or even a routing-application
* a professional-editing tool like JOSM or Merkaator

Instructions are on the
[https://github.com/MarcusWolschon/osmeditor4android/wiki wiki].
.

Repo Type:git
Repo:https://github.com/MarcusWolschon/osmeditor4android

Build:0.7.0,14
    commit=201

Build:0.8.1r402,17
    commit=404
    srclibs=ActionBarSherlock@4.1.0
    extlibs=android/android-support-v4.jar
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-14

Build:0.8.2r416,18
    commit=416
    srclibs=ActionBarSherlock@4.1.0
    extlibs=android/android-support-v4.jar
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-14

Build:0.8.3r419,19
    commit=419
    srclibs=ActionBarSherlock@4.2.0
    extlibs=android/android-support-v4.jar
    prebuild=sed -i 's@\.1=.*@.1=$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-14

Build:0.9.3r677,25
    commit=0.9/677
    srclibs=1:ActionBarSherlock@4.4.0
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-19

Build:0.9.4r681,26
    commit=0.9/681
    srclibs=1:ActionBarSherlock@4.4.0
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-19

Build:0.9.4r698,27
    commit=0.9/698
    srclibs=1:ActionBarSherlock@4.4.0
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-19

Build:0.9.4r741,28
    commit=0.9/741
    srclibs=1:ActionBarSherlock@4.4.0
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-19

Build:0.9.4r745,29
    commit=0.9/745
    srclibs=1:ActionBarSherlock@4.4.0
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-19

Build:0.9.4r770,30
    commit=0.9/770
    srclibs=1:ActionBarSherlock@4.4.0
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-19

Build:0.9.4r781,31
    commit=0.9/781
    srclibs=1:ActionBarSherlock@4.4.0
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    target=android-19

Build:0.9.4r828,32
    commit=0.9/828
    srclibs=1:ActionBarSherlock@4.4.0
    extlibs=android/android-support-v4.jar
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

# start using new github repo
Build:0.9.5.1.965,44
    commit=0.9.5.1.965
    gradle=yes
    rm=libs/acra*.jar,libs/xmpcore*.jar,libs/signpost*.jar
    prebuild=sed -i -e '/support-v4/acompile "ch.acra:acra:4.5.0"\ncompile "com.adobe.xmp:xmpcore:5.1.2"\ncompile "oauth.signpost:signpost-core:1.2.1.2"\ncompile "oauth.signpost:signpost-commonshttp4:1.2.1.2"' -e '/versionCode/d' -e '/versionName/d' build.gradle

Build:0.9.6.1.990,48
    commit=0.9.6
    gradle=yes
    rm=libs/acra*.jar,libs/xmpcore*.jar,libs/signpost*.jar
    prebuild=sed -i -e '/support-v4/acompile "ch.acra:acra:4.5.0"\ncompile "com.adobe.xmp:xmpcore:5.1.2"\ncompile "oauth.signpost:signpost-core:1.2.1.2"\ncompile "oauth.signpost:signpost-commonshttp4:1.2.1.2"' -e '/versionCode/d' -e '/versionName/d' build.gradle

# upstream messed up versioning. unclear if this is 0.9.6... or 0.9.7-beta
Build:0.9.6.1.1026-fdroid,55
    commit=4d6b561602fbe765c50b04668eeece077c856fa4
    gradle=yes
    forceversion=yes
    rm=libs/acra*.jar,libs/xmpcore*.jar,libs/signpost*.jar
    prebuild=sed -i -e '/support-v4/acompile "ch.acra:acra:4.5.0"\ncompile "com.adobe.xmp:xmpcore:5.1.2"\ncompile "oauth.signpost:signpost-core:1.2.1.2"\ncompile "oauth.signpost:signpost-commonshttp4:1.2.1.2"' -e '/versionCode/d' -e '/versionName/d' build.gradle && \
        sed -i -e 's/Vespucci/Vespucci (F-Droid)/g' res/values/appname.xml

Maintainer Notes:
* Remove versionCode and versionName from build.gradle due to mismatch (using
  higher VC in AndroidManifest.xml):
  https://github.com/MarcusWolschon/osmeditor4android/issues/286
* Cannot replace JAR file with com.drewnoakes:metadata-extractor:2.8.1:
  https://github.com/MarcusWolschon/osmeditor4android/issues/285
* Bump ArchivePolicy with each new-repo build until back to normal.
* Upstream requires -fdroid specific appname
.

Archive Policy:3 versions
Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.9.6.1.1026
Current Version Code:55
