Categories:Multimedia
License:GPLv3+
Web Site:
Source Code:https://github.com/einmalfel/PodListen
Issue Tracker:https://github.com/einmalfel/PodListen/issues

Auto Name:PodListen
Summary:Podcast player with lightweight interface
Description:
Main idea behind this app is to set podcast listening free from inconvenience of
switching between screens and going through menus. Instead, all navigation in
PodListen is done by simply swiping between 3 tabs, basic actions are done with
single tap.

PodListen is open-source ad-free app, it doesn't include analytics services,
collect any user data or use strange system permissions.

Besides original interface, it has basic features one can expect from podcatcher
app:

* feeds refresh and episodes download
* OPML import and export
* subscription by clicking on RSS links in browser
* home screen widget
* playlist sorting modes
* preferences like “download on wi-fi only”

Podcast search feature is planned for an update.
.

Repo Type:git
Repo:https://github.com/einmalfel/PodListen

Build:1.0,1
    commit=1.0
    subdir=app
    gradle=yes

Build:1.0.1,10101
    commit=1.0.1
    subdir=app
    gradle=yes

Build:1.1.2,10102
    commit=1.1.2
    subdir=app
    gradle=yes

Build:1.1.3,10103
    commit=1.1.3
    subdir=app
    gradle=yes

Build:1.1.4,10104
    commit=1.1.4
    subdir=app
    gradle=yes

Build:1.2.0,10200
    commit=1.2.0
    subdir=app
    gradle=yes

Build:1.2.1,10201
    commit=1.2.1
    subdir=app
    gradle=yes

Build:1.2.3,10203
    commit=1.2.3
    subdir=app
    gradle=yes

Build:1.2.4,10204
    commit=1.2.4
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.2.4
Current Version Code:10204
