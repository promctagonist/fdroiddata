Categories:Games
License:GPLv3
Web Site:https://github.com/caarmen/nounours-android/blob/HEAD/README.md
Source Code:https://github.com/caarmen/nounours-android
Issue Tracker:https://github.com/caarmen/nounours-android/issues

Auto Name:Nounours
Summary:Nounours the teddy bear
Description:
This cute teddy bear is a fun friend for children of all ages. Move him around
by moving his head, paws, tummy, and ears. Spin him around.  Shake the phone.
Nounours does some funny animations (disco, flying, jumping). Record and share
his moves.

Includes themes of Nounours in different settings, as well as a theme of
Bugdroid.

Includes a live wallpaper.
.

Repo Type:git
Repo:https://github.com/caarmen/nounours-android

Build:2.0.1,201
    commit=release-2.0.1
    subdir=app
    gradle=yes

Build:2.0.2,202
    commit=release-2.0.2
    subdir=app
    gradle=yes

Build:2.1.0,210
    disable=scanner errors
    commit=release-2.1.0
    subdir=app
    gradle=yes

Build:2.2.1,221
    commit=release-2.2.1
    subdir=app
    gradle=full

Build:2.2.2,222
    commit=release-2.2.2
    subdir=app
    gradle=full

Build:2.3.2,232
    commit=release-2.3.2
    subdir=app
    gradle=full

Build:2.3.3,233
    commit=release-2.3.3
    subdir=app
    gradle=full

Build:3.0.0,300
    commit=release-3.0.0
    subdir=handheld
    gradle=fullFoss
    scanignore=wear/build.gradle

Build:3.0.1,301
    commit=release-3.0.1
    subdir=handheld
    gradle=fullFoss
    scanignore=wear/build.gradle

Auto Update Mode:Version release-%v
Update Check Mode:Tags ^release-
Current Version:3.0.1
Current Version Code:301
