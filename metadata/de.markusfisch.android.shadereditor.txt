Categories:Development
License:MIT
Web Site:http://markusfisch.de
Source Code:https://github.com/markusfisch/ShaderEditor
Issue Tracker:https://github.com/markusfisch/ShaderEditor/issues

Auto Name:Shader Editor
Summary:Create and edit GLSL shaders
Description:
Create and edit GLSL shaders on your Android phone or tablet and use them as
live wallpaper.
.

Repo Type:git
Repo:https://github.com/markusfisch/ShaderEditor.git

Build:1.6.3,12
    commit=1.6.3

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.6.3
Current Version Code:12
