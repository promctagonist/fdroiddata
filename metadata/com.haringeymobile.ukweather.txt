Categories:Time
License:Apache2
Web Site:
Source Code:https://github.com/Kestutis-Z/World-Weather
Issue Tracker:https://github.com/Kestutis-Z/World-Weather/issues

Auto Name:World Weather
Summary:View weather forecast
Description:
Displays current weather and forecasts from OpenWeatherMap. Two kinds of
forecast are available: 14-day daily weather and 5-day three-hourly weather. You
can select any of the 200,000 cities supported by OpenWeatherMap.
.

Repo Type:git
Repo:https://github.com/Kestutis-Z/World-Weather

Build:1.1.1,15
    commit=37a90fb46cb5fd99fc634a1ab325e1bd7b296765
    subdir=WorldWeather/app
    gradle=yes

Build:1.1.3,17
    commit=7eee764b70832db82d63838e081c6a04eecb4e7e
    subdir=WorldWeather/app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.3
Current Version Code:17
