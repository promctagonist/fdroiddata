Categories:Multimedia
License:GPLv2
Web Site:https://photobackup.github.io/
Source Code:https://github.com/PhotoBackup/client-android
Issue Tracker:https://github.com/PhotoBackup/client-android/issues
Changelog:https://github.com/PhotoBackup/client-android/blob/HEAD/CHANGELOG.md

Auto Name:PhotoBackup
Summary:Upload photos on-the-fly
Description:
Always running silently in background, this service sends photos to the
associated server as soon as you take them. An upload journal allows you to see
the status of each photo in your device. It does not backup videos.

A [https://github.com/PhotoBackup/server-bottle server implementation] is also
available.
.

Repo Type:git
Repo:https://github.com/PhotoBackup/client-android

Build:0.6.1,10
    commit=ece3725e5d09e8c007c8782e5cbee04e33a5da9b
    gradle=yes

Build:0.6.2,11
    commit=8b37bbb1e085d3f359c9c622ba987f5bef538f22
    gradle=yes

Build:0.6.3,12
    commit=v0.6.3
    gradle=yes

Build:0.6.4,13
    commit=v0.6.4
    gradle=yes

Build:0.6.5,14
    commit=v0.6.5
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.6.5
Current Version Code:14
